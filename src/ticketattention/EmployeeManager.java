/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ticketattention;

import java.util.ArrayList;

/**
 *
 * @author aleja
 */
public class EmployeeManager {
   private static ArrayList<Employee> employees;
   private static EmployeeManager instance;
   
   public EmployeeManager(){
       employees = null;
       employees = employeeGenerator();
   }
   
   public static EmployeeManager getInstance(){
       if (instance ==null){
           instance = new EmployeeManager();
       }
       return instance;
   }
   public ArrayList<Employee> getEmployees(){
       return employees;
   }
   
   public Employee getEmployee(String id){
       int index = -1 ;
       for ( int i = 0 ; i  < employees.size();  i ++ ){
           Employee e = employees.get(i);
           if ( id.equals(e.getId())){
               index =  i;
           }
       }
       return employees.get(index);
   }
   
   public void addEmployee(Employee employee){
       employees.add(employee);
   }
   
   public void deleteEmployee(String pId){
       for ( int i = 0 ; i  < employees.size();  i ++ ){
           Employee e = employees.get(i);
           if ( pId.equals(e.getId())){
               employees.remove(i);
           }
       }
   }
   
   public void updateEmployee(Employee e){
       for ( int i = 0 ; i  < employees.size();  i ++ ){
           if ( e.getId().equals(employees.get(i).getId())){
               employees.remove(i);
               employees.add(e);
           }
       }
       
   }
   
   public boolean isAdmin(Employee e){
       for ( int i = 0 ; i  < employees.size();  i ++ ){
           if ( e.getId().equals(employees.get(i).getId())){
               return e.isSupervisor();
           }
       }return false;
   }
   
   public int employeeLogin(String userId,  String pass){
        int empIndex = -1;
        for ( int i = 0 ; i  < employees.size();  i ++ ){
            if ( userId.equals(employees.get(i).getId()))empIndex =  i;
        }
        if (empIndex != -1){
            Employee e = getEmployee(userId);
            if (e.getPassword().equals(pass)){
                if (e.isSupervisor()) return 1;
                else return 0;
            }else return -1;
        }else{
            return -1;
        }
   } 
   
   public static ArrayList<Employee> employeeGenerator(){
        ArrayList<Employee> emp = new ArrayList<> ();
        Employee admin = new Employee(0, "Alejandro Rojas", "ar@email.com", "pass1", "arojas34", true );
        emp.add(admin);
        for ( int i = 0 ; i < 6 ; i++){
             Employee user = new Employee(0, "Usuario "+String.valueOf(i), "user"+String.valueOf(i)+"@email.com", "pass"+String.valueOf(i), "user"+String.valueOf(i), false );
             user.setCategory(Category.Red);
             emp.add(user);  
        }
        for ( int i = 6; i < 10 ; i++){
             Employee user = new Employee(0, "Usuario "+String.valueOf(i), "user"+String.valueOf(i)+"@email.com", "pass"+String.valueOf(i), "user"+String.valueOf(i), false );
             user.setCategory(Category.Yellow);
             emp.add(user);  
        }
        for ( int i = 10; i < 13 ; i++){
             Employee user = new Employee(0, "Usuario "+String.valueOf(i), "user"+String.valueOf(i)+"@email.com", "pass"+String.valueOf(i), "user"+String.valueOf(i), false );
             user.setCategory(Category.Green);
             emp.add(user);  
        }
        return emp;
    }
    
}
