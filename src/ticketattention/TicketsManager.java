/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ticketattention;


import java.util.concurrent.ThreadLocalRandom;
import com.sun.java.swing.plaf.windows.WindowsBorders;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author familia
 */
public class TicketsManager {
    
    private static ArrayList<Ticket> ticketsReceived;
    private static TicketsManager instance;
   
    public TicketsManager() {
        ticketsReceived =  ticketsGenerator();
    }
    
    public static TicketsManager getInstance(){
       if (instance ==null){
           instance = new TicketsManager();
       }
       return instance;
   }

    public ArrayList<Ticket> getTicketsReceived() {
        return ticketsReceived;
    }
 
    public  Ticket getTicketId(String pId){

      Ticket pTicket =  new Ticket();
      for (Ticket t:ticketsReceived) {
            if (t.getId().equals(pId)){
                pTicket = t;
            }                  
        }
      return pTicket;
    }

    public void setTicketsReceived( ArrayList<Ticket> pTickets) {
        this.ticketsReceived = pTickets;
    }
   
    public boolean assignTicket(String pTicketId, Employee pEmployee){
        Ticket ticket = searchTicket(pTicketId);
        if (ticket.getId()!= null && ticket.getCategory() == pEmployee.getCategory()){
            ticket.setAttendant(pEmployee);
            return true;
        }   

        return false;
    }
    
    public boolean addTicket(Ticket pNewTicket){
       return ticketsReceived.add(pNewTicket);
    }
    
    
    public ArrayList<Ticket> getTicketsByEmployee(String pEmployeeId){
        ArrayList<Ticket> filteredTickets = new ArrayList();
          for (Ticket ticket:ticketsReceived) {

              if ( ticket.getAttendant() != null && ticket.getAttendant().getId().equals(pEmployeeId)){
                  filteredTickets.add(ticket);
              }
          }
          System.out.println("getTicketsByEmployee: Size return: "+filteredTickets.size());
          return filteredTickets;
    }
    
    public Category getMostUsedTicket(){


        int catGreen=0, catYellow =0, catRed = 0;
        for (Ticket ticket:ticketsReceived) {
              if (ticket.getCategory() == Category.Green)   catGreen ++;
              if (ticket.getCategory() == Category.Yellow)  catYellow ++;
              if (ticket.getCategory() == Category.Red )    catRed ++;
          }
        if (catGreen > catYellow ){
            if (catGreen > catRed) return Category.Green;
            else return Category.Red;
        }else{
            if (catYellow > catRed) return Category.Yellow;
            else return Category.Red;
        }
    }
    
    
    public void setTicketUrgency(Ticket pTicket, Category color){
        for (Ticket ticket:ticketsReceived) {
            if (ticket.getId().equals(pTicket.getId())){
                ticket.setCategory(color);
            }
        }
        
    }
    

    public  boolean receiveTicket(Ticket pNewTicket){
       return ticketsReceived.add(pNewTicket);
    }
    
    public String consultTicket(String pId){
        for (Ticket ticket:ticketsReceived) {
            if (ticket.getId().equals( pId))
                return ticket.toString();
        }
        return "Ticket not found";
    }

    public  void deleteTicket(String pTicketId){
        int index =0;
        int deleteIndex = -1;
        for (Ticket t:ticketsReceived) {
            if (t.getId().equals(pTicketId)){
                deleteIndex = index;
            }
            index ++; 
        }
        if(deleteIndex != -1) ticketsReceived.remove(deleteIndex);
        
    }
    
   

    public boolean classifyTicket(String pTicketId, String pCategory){
        Ticket ticket = searchTicket(pTicketId);
        Category newTicketCategory = null;
        
        if (!ticket.getId().isEmpty()){
            newTicketCategory = searchCategory(pCategory);
            ticket.setCategory(newTicketCategory);
            return true;
        }
        
        return false;
    }
    
    public boolean modifyTicketState(String pTicketId, String pState){
        
        Ticket ticket = searchTicket(pTicketId);
        State newTicketState = null;
        
        if (!ticket.getId().isEmpty()){
            newTicketState = searchState(pState);
            ticket.setState(newTicketState);
            return true;
        }
        
        return false;
        
    }
    
    public Ticket searchTicket(String pTicketId){

        for (Ticket ticket:ticketsReceived){
            if  (ticket.getId().equals(pTicketId))
                return ticket;
        }
        Ticket emptyTicket = new Ticket();
        return emptyTicket;
    }
    
    public Category searchCategory(String pCategory){
            
        for (Category category:Category.values()){
            if (category.name().equals(pCategory))
                return category;
        }
        Category invalidCategory = null;
        return invalidCategory.Invalid;
    }
    
    public State searchState(String pState){
        for (State state:State.values()){
            if (state.name().equals(pState))
                return state;
        }
        State invalidState = null;
        return invalidState.Invalid;
    }
    
   
    public ArrayList<Category> getTicketsCategories(){
        ArrayList<Category> categoriesFound = new ArrayList<Category>();
        for(Ticket ticket:ticketsReceived){
            Category ticketCategory = ticket.getCategory();
            categoriesFound.add(ticketCategory);
        }
        
        return categoriesFound;
    }
  
    public void getTicketsPerDate(String year,String month,String day){
        Date date = parseDate(year, month, day);
        ArrayList<Ticket> ticketsFound = findTicketsOnADate(date);
        ticketsFound = sortTicketsPerCategory(ticketsFound);
        printTickets(ticketsFound);
    }
    
    public ArrayList<Ticket> findTicketsOnADate(Date date){
        ArrayList<Ticket> ticketsFound = new ArrayList<Ticket>();
        for (Ticket ticket: ticketsReceived){
            if (ticket.getDate() == date )
                ticketsFound.add(ticket);  
        }
        return ticketsFound;
    }
    
    public ArrayList<Ticket> sortTicketsPerCategory(ArrayList<Ticket> tickets){
        Collections.sort(tickets, Ticket.ticketCategoryComparator);
        
        return tickets;
    }
    
    public static Date parseDate(String year,String month,String day){
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = null;
            try {
                date = dateFormat.parse(day+"/"+month+"/"+year);
            } catch (ParseException ex) {
                Logger.getLogger(TicketsManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        return date;
    }
    
    public void printTickets(ArrayList<Ticket> tickets){
        for(Ticket ticket: tickets){
            ticket.toString();
        }
        System.out.println("Done");
    }


    public static ArrayList<Ticket> ticketsGenerator(){
        ArrayList<Ticket> tickets = new ArrayList<> ();
        ArrayList<String> names = new ArrayList<>();
        names.add("Pedro");names.add("Juan");names.add("Lucia");names.add("Andrea");
        names.add("Luis");names.add("Maria");names.add("Carlos");names.add("Julian");
        
        for ( int i = 0 ; i < 6 ; i++){
            Ticket ticket =  new Ticket();
            ticket.setCategory(Category.Red);
            ticket.setId("000"+i);
            ticket.setDate(parseDate("2016","1","1"));
            ticket.setSubject("El sistema se Cae");
            ticket.setClientName(names.get(ThreadLocalRandom.current().nextInt(0, names.size())));  
            tickets.add(ticket);
        }
        for ( int i = 6 ; i < 10 ; i++){
            Ticket ticket =  new Ticket();
            ticket.setCategory(Category.Yellow);
            ticket.setId("000"+i);
            ticket.setDate(parseDate("2016","1","1"));
            ticket.setSubject("La computadora prende fuego");
            ticket.setClientName(names.get(ThreadLocalRandom.current().nextInt(0, names.size())));  
            tickets.add(ticket);
        }
        for ( int i = 10 ; i < 14 ; i++){
            Ticket ticket =  new Ticket();
            ticket.setCategory(Category.Green);
            ticket.setId("000"+i);
            ticket.setDate(parseDate("2016","1","1"));
            ticket.setSubject("No salen los Asientos");
            ticket.setClientName(names.get(ThreadLocalRandom.current().nextInt(0, names.size())));  
            tickets.add(ticket);
        }
        for ( int i = 14 ; i < 18 ; i++){
            Ticket ticket =  new Ticket();
            ticket.setCategory(Category.None);
            ticket.setId("000"+i);
            ticket.setDate(parseDate("2016","1","1"));
            ticket.setSubject("No sale mi foto de perfil");
            ticket.setClientName(names.get(ThreadLocalRandom.current().nextInt(0, names.size())));  
            tickets.add(ticket);
        }
        return tickets;
    }
}
    
