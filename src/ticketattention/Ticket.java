/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ticketattention;

import java.util.Date;
import java.util.Comparator;

/**
 *
 * @author familia
 */
public class Ticket {
    
    private Category category;

    private Date date;
    private String clientName,subject, id;
    private State state;
    private Employee attendant;

    public Ticket(Category category, Date date, String clientName,
    String subject, String id, State state, Employee attendant) {
        
        this.category = category;
        this.date = date;
        this.clientName = clientName;
        this.subject = subject;
        this.id = id;
        this.state = state;
        this.attendant = attendant;
    }
     public Ticket(){
         
     }
    

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {

        this.date = date;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Employee getAttendant() {
        return attendant;
    }

    public void setAttendant(Employee attendant) {
        this.attendant = attendant;
    }
    

     public static Comparator<Ticket> ticketCategoryComparator = new Comparator<Ticket>() {

	public int compare(Ticket firstTicket, Ticket secondTicket) {
	   String firstTicketCategory = firstTicket.getCategory().name().toUpperCase();
	   String secondTicketCategory = secondTicket.getCategory().name().toUpperCase();

	   //ascending order
	   return firstTicketCategory.compareTo(secondTicketCategory);

	   //descending order
	   //return StudentName2.compareTo(StudentName1);
    }};

    @Override
    public String toString(){
        
        String ticketInformation = "";
        String pCategory = "Category: " + this.getCategory().name() + "/n";
        String pDate = "Date: " + this.getDate() + "/n";
        String pClientName = this.getClientName();
        String pSubject = this.getSubject();
        String pId = this.getId();
        String pState = "State: " + this.getState().name() + "/n";
        String pAttendant = "Asigned to: " + this.getAttendant().getName()+ "/n";
        ticketInformation += pCategory  + pDate + pClientName + pSubject + pId + pState + pAttendant;
        return ticketInformation;
    }
    
    public boolean equals(Ticket t){
        return t.id.equals(this.id);
    }
    
}