/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ticketattention;

/**
 *
 * @author familia
 */
public class Employee {
    
    private float attentionPercentage;
    private String name, email, password, id;
    private boolean isSupervisor;
    private Category category;

    public Employee(float attentionPercentage, String name, String email,
    String password, String id, boolean isSupervisor) {
        
        this.attentionPercentage = attentionPercentage;
        this.name = name;
        this.email = email;
        this.password = password;
        this.id = id;
        this.isSupervisor = isSupervisor;
        //this.category = category;

    }
    
    public float getAttentionPercentage() {
        return attentionPercentage;
    }

    public void setAttentionPercentage(float attentionPercentage) {
        this.attentionPercentage = attentionPercentage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isSupervisor() {
        return isSupervisor;
    }

    public void setIsSupervisor(boolean isSupervisor) {
        this.isSupervisor = isSupervisor;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
    
}
